<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
            crossorigin="anonymous"></script>
</head>
<body>
<div style="
        width: 1000px;
        height: 500px;
        margin: 7px;
        border: 1px solid gray;
        padding: 10px;">
    <div class="container">
        <h1>Anschrift</h1>
        <form action="Kontakt.php" method="post">
            <div class="mb-3">
                <label for="input-feld">Strasse</label>
                <input type="text" id="input-feld" class="form-control" name="strasse">
            </div>
            <div class="mb-3">
                <label for="input-feld">PLZ / Ort</label>
                <input type="text" id="input-feld" class="form-control" name="plz">
            </div>
            <div class="mb-3">
                <label for="input-feld">Land</label>
                <input type="text" id="input-feld" class="form-control" name="land">
            </div>
            <button type="submit"  class="btn btn-primary" name="btn" value="2">weiter</button>
        </form>

    </div>

</div>
<div class="container">
    <ul class="bd-footer-links ps-0 mb-3">
        <li class="d-inline-block"><a href="index.html">Home</a></li>
        <li class="d-inline-block ms-3"><a href="PersönlicheDaten.php">Persönliche Daten</a></li>
        <li class="d-inline-block ms-3"><a href="Anschrift.php">Anschrift</a></li>
        <li class="d-inline-block ms-3"><a href="Firma.php">Firma</a></li>
    </ul>
</body>
</html>



